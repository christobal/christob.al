/*
TODO:
Switch out setTimeout with some Listener that listens for DOM change.
Possibility of subscribing to matrix room ID's
If post title exist, dont create duplicate.

*/



/**
 * Example use:
 * $('div:icontains("Text in page")');
 * Will return jQuery object containing any/all of the following:
 * <div>text in page</div>
 * <div>TEXT in PAGE</div>
 * <div>Text in page</div>
 * https://gist.github.com/pklauzinski/b6f836f99cfa11100488
 */
$.expr[':'].icontains = $.expr.createPseudo(function(text) {
  return function(e) {
      return $(e).text().toUpperCase().indexOf(text.toUpperCase()) >= 0;
  };
});

$.expr[':'].inot = $.expr.createPseudo(function(text) {
  return function(e) {
      return $(e).text().toUpperCase().indexOf(text.toUpperCase()) < 0;
  };
});


var listTitles = function() {
  posts = document.getElementsByTagName("h1")
  archiveItems = document.getElementsByClassName("archive-item")
  archiveList = document.getElementById("archive-list")
  if (archiveItems.length > 0 && posts.length === archiveItems.length) {
    // List is up to date
    // console.log("Archive up to date")
    // This should probably be prettier
  } else
  $(".archive-item").remove()
  for (i = 0; i < posts.length; i++) {

    if (posts.length > 0 && archiveItems.length === 0) {
      // console.log("Initialize List")
      initLi()
    }

    else if (archiveItems.length >= 0 && posts.length !== archiveItems.length) {
      // console.log("Adding New Post")
      initLi()
      
    }

  }
  setTimeout(listTitles, 2000) 
}  

var initLi = function() {
      
      let titleLi = document.createElement("li")
      titleLi.className = "archive-item"
      titleLi.innerHTML = posts[i].innerHTML
      titleLi.onclick = function() {
      // Clicking on the Title will filter out other posts
      filter(titleLi.innerHTML)
    }
      archiveList.appendChild(titleLi)
  }  

var filter = function(searchText) {
    searchIs = $('h1:icontains("'+searchText+'")')
    searchIsNot = $('h1:inot("'+searchText+'")')

    for (i = 0; i < searchIsNot.length; i++) {
      $(searchIsNot[i].parentElement.parentElement).hide()
    }
    for (i = 0; i < searchIs.length; i++) {
      $(searchIs[i].parentElement.parentElement).show()
    }
}

var init = function() {
  $(document).ready(listTitles())
  
}
init()

### Matrix Live Blog

https://christob.al



#### All credit goes to Matrix-Live, all I did was make a different visual wrapper.

[Source](https://gitlab.com/argit/matrix-live)

[Web](https://live.hello-matrix.net/)


#### Basic functionality: 

* Basic HTML/CSS
* Simple JS/jQuery DOM manipulation
* Matrix-Live Integration
* Mastofeed Integration


#### Future plans:

- [x] Move hosting from gitlab pages to dedicated host (Thank you [CloudVault](https://cloudvault.me/))
- [x] Get lets encrypt cert
- [ ] Ability to add new pages(subscribe to matrix rooms) with a matrix room id
- [x] Add mastodon stream to page, almost there. Customized [Mastofeed](https://www.mastofeed.com) to adapt to my css)
- [x] Add some sort of archive/filtering to get to individual posts
- [x] Make mobile and view change adaptive/reposive

#### How to add Matrix Live to your own page?

Add this to whatever div you want the stream to appear in:
Initial load is the number of posts/entries to load.

```
      <matrix-live homeserver="https://matrix.org"
             room="!yourroomid:yourhost.org"
             initial-load="120"> 
      </matrix-live>

```

Then add the following to the bottom of your HTML. (or host them yourself)

```
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="https://live.hello-matrix.net/matrix-live-min.js"></script>

```